const express = require('express');
const path = require('path');

const session = require('express-session');
const SessionStore = require('connect-mongodb-session')(session);
const flash = require('connect-flash');

const homeRouter = require('./routes/home.route');
const productRouter = require('./routes/product.route');
const authRouter = require('./routes/auth.route');
const cartRouter = require('./routes/cart.route');
const adminRouter = require('./routes/admin.route');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views'); //default
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'images')));
app.use(flash());

const STORE = new SessionStore({
    // uri: "mongodb+srv://ahly:Hnasser12345@cluster0-vzxsg.mongodb.net/online-shop?retryWrites=true&w=majority",
    uri: 'mongodb://localhost:27017/online_shop',
    collection: 'sessions'
})

app.use(session({
    secret: 'encryption is loading !!!',
    saveUninitialized: false,
    resave: false,
    store: STORE
}))
app.use(authRouter);
app.use(homeRouter);
app.use(productRouter);
app.use(cartRouter);
app.use(adminRouter);

app.get('/error', (req, res, next) => {
    res.status(500);
    res.render('error.ejs', {
        isUser: req.session.userId,
        isAdmin: req.session.isAdmin
    });
});

app.get('/not-admin', (req, res, next) => {
    res.status(403);
    res.render('not-admin.ejs', {
        isUser: req.session.userId,
        isAdmin: false
    });
})

app.use((error, req, res, next) => {
    res.redirect('/error');
})

app.use((req, res, next) => {
    res.status(404);
    res.render('pageNotFound.ejs', {
        isUser: req.session.userId,
        isAdmin: req.session.isAdmin,
        pageTitle: 'Page Not Found'
    })

})

const port = 3300;
app.listen(port, (err) => {
    console.log('server is running on port 3300');
})


