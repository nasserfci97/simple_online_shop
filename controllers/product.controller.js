const productModel = require('../models/product.model');

exports.getProduct = (req, res, next) => {

    productModel.getFirstProduct()
    .then(product => {
        res.render('product', {
            product: product,
            isUser: req.session.userId,
            isAdmin: req.session.isAdmin,
            pageTitle: 'Products',
        })
    })
}

exports.getProductById = (req, res, next) => {
    let id = req.params.id;
    productModel.getProductById(id)
    .then(product => {
        res.render('product', {
            product: product,
            isUser: req.session.userId,
            isAdmin: req.session.isAdmin,
            pageTitle: 'Product',
        })
    })
}

