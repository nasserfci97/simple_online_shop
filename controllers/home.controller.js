const productModel = require('../models/product.model');

exports.getHome = (req, res, next) => {

    let categoryState = req.query.category;
    let productPromise;
    let validCategories = ['clothes', 'phones', 'laptops', 'tablets'];

    if( categoryState && validCategories.includes(categoryState) ) productPromise =  productModel.getProductByCategory(categoryState);
     else productPromise = productModel.getAllProducts();

      productPromise.then(products => {
            res.render('index', {
                products: products,
                isUser: req.session.userId,
                isAdmin: req.session.isAdmin,
                pageTitle: 'Home',
                validationErrors: req.flash('validationErrors')[0]
            })
        })
    
}