const validationResult = require('express-validator').validationResult;

const productModel = require('../models/product.model');

exports.getAdd = (req, res, next) => {
    res.render('addProduct', {
        isUser: true,
        isAdmin: true,
        pageTitle: 'Add Product',
        validationErrors: req.flash('validationErrors')
    })
}

exports.postAdd = (req, res, next) => {

    if (validationResult(req).isEmpty()){
        req.body.image = req.file.filename;
        productModel
        .addNewProduct(req.body)
        .then((result) => {
            req.flash('added', true);
            res.redirect('/add');
        })
        .catch((err) => { 
            next(err);
        })
     } else {
            req.flash('validationErrors', validationResult(req).array());
            res.redirect('admin/add');
     }
}