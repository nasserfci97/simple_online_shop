const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

// const DB_URL =  "mongodb+srv://ahly:Hnasser12345@cluster0-vzxsg.mongodb.net/online-shop?retryWrites=true&w=majority";
const DB_URL = 'mongodb://localhost:27017/online_shop';

const userSchema = mongoose.Schema({
  username: String,
  email: String,
  password: String,
  isAdmin: {
      type: Boolean,
      default: false
  }
});

const User = mongoose.model('user', userSchema);

exports.createUser = (username, email, password) => {

    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            return User.findOne({ email: email });
        })
        .then(user => {
            if(user){
                mongoose.disconnect();
                reject('email is exited');
            } else {
                return bcrypt.hash(password, 10);
            }
        })
        .then(hashedPassword => {
            let user = new User({
                username: username,
                password: hashedPassword,
                email: email
            })
            return user.save();
        })
        .then(() => {
            mongoose.disconnect();
            resolve('user has been recoreded in database');
        })
        .catch(err => {
            mongoose.disconnect();
            reject(err);
        })
    })
  
}

exports.login = (email, password) => {
    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            return User.findOne({email: email});
        })
        .then(user => {
            if(!user){
                mongoose.disconnect();
                reject('wrong email');
            } else {
                 bcrypt.compare(password, user.password)
                 .then(same => {
                    if(!same){
                        mongoose.disconnect();
                        reject('wrong password')
                    } else {
                        mongoose.disconnect();
                        resolve({
                            id: user._id,
                            isAdmin: user.isAdmin
                        });
                    }
                })
            }
        })
        .catch(err => {
            mongoose.disconnect();
            reject(err);
        })
    }
)}