const mongoose = require('mongoose');

// const DB_URL =  "mongodb+srv://ahly:Hnasser12345@cluster0-vzxsg.mongodb.net/online-shop?retryWrites=true&w=majority";

const DB_URL = 'mongodb://localhost:27017/online_shop';

const productSchema = mongoose.Schema({
    name: String,
    price: Number,
    category: String,
    description: String,
    image: String
});

const Product = mongoose.model('product', productSchema);

exports.getAllProducts = () => {
    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            return Product.find({})
        })
        .then(products => {
            mongoose.disconnect();
            resolve(products);
        })
        .catch(err => reject(err))
    })
 
}

exports.getProductByCategory = (category) => {
    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            return Product.find({ category: category })
        })
        .then(products => {
            mongoose.disconnect();
            resolve(products);
        })
        .catch(err => reject(err))
    })
}
  
exports.getProductById = (id) => {

    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            return Product.findById(id);
        })
        .then(product => {
            mongoose.disconnect();
            resolve(product);
        })
        .catch(err => reject(err))
    })
}

exports.getFirstProduct = () =>{

    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            return Product.findOne({});
        })
        .then(product => {
            mongoose.disconnect();
            resolve(product);
        })
        .catch(err => reject(err))
    })
}

exports.addNewProduct = (data) => {
    return new Promise((resolve, reject) => {
       // return reject('error');
        mongoose
        .connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .then(() => {
            let newProduct = new Product(data);
           return newProduct.save();
        })
        .then(product => {
            mongoose.disconnect();
            resolve(product);
        })
        .catch(err => {
            mongoose.disconnect();
            reject(err);
        })
    })
}