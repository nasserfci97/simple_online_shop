const mongoose = require('mongoose');

// const DB_URL =  "mongodb+srv://ahly:Hnasser12345@cluster0-vzxsg.mongodb.net/online-shop?retryWrites=true&w=majority";

const DB_URL = 'mongodb://localhost:27017/online_shop';

const cartSchema = mongoose.Schema({
    name: String,
    price: Number,
    amount: Number,
    productId: String,
    userId: String,
    timestamp: Number
});

const CartItem =  mongoose.model('cart', cartSchema);


exports.editItem = (cartId, newData) => {
    return new Promise((resolve, reject, next) => {
        mongoose
        .connect(
            DB_URL,
            { useNewUrlParser: true, useUnifiedTopology: true  } 
         )
         .then(() => {
             return CartItem.updateOne({_id: cartId}, newData);
         })
         .then(() => {
             mongoose.disconnect();
             resolve();
         })
         .catch(err => {
             mongoose.disconnect();
             reject(err);
         })
    })
}
exports.getCartByUserId = userId => {
    return new Promise((resolve, reject, next) => {
        mongoose
        .connect(
            DB_URL,
            { useNewUrlParser: true, useUnifiedTopology: true  } 
         )
         .then(() => {
             return CartItem.find(
                 {userId: userId},
                 {},
                 {sort: {timestamp: 1}});
         })
         .then(items => {
             mongoose.disconnect();
             resolve(items);
         })
         .catch(err => {
             mongoose.disconnect();
             reject(err);
         })
    })
}

exports.addItem = data => {
    return new Promise((resolve, reject) => {
        mongoose
        .connect(
            DB_URL,
             { useNewUrlParser: true, useUnifiedTopology: true  } )
        .then(() => {
            // return items with matchingUserid and productId
            // if existed then change amount and update
            // else add a new item
            return CartItem.findOne({userId: data.userId, productId: data.productId});
        })
        .then(item => {
           console.log(item);
            if (item) {
                data.amount = parseInt(data.amount) + parseInt(item.amount);
                return CartItem.updateOne({productId: data.productId}, data);
            }
            else {
                let item = new CartItem(data);
                return item.save();
            }
        
        })
        .then((item) => {
            mongoose.disconnect();
            resolve();
        })
        .catch((err) => {
            mongoose.disconnect();
            reject(err);
        })
    })
}

exports.deleteItem = (id) => {
    return new Promise((resolve, reject, next) => {
        mongoose
        .connect(
            DB_URL,
            { useNewUrlParser: true, useUnifiedTopology: true  } 
         )
         .then(() => {
             return CartItem.findByIdAndDelete(id);
         })
         .then(() => {
             mongoose.disconnect();
             resolve();
         })
         .catch(err => {
             mongoose.disconnect();
             reject(err);
         })
    })
}

exports.deleteAllProducts = (id) => {
    return new Promise((resolve, reject, next) => {
        mongoose
        .connect(
            DB_URL,
            { useNewUrlParser: true, useUnifiedTopology: true  } 
         )
         .then(() => {
             return CartItem.deleteMany({userId: id});
         })
         .then(() => {
             mongoose.disconnect();
             resolve();
         })
         .catch(err => {
             mongoose.disconnect();
             reject(err);
         })
    })
}