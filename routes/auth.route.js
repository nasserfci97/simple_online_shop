const router = require('express').Router();
const bodyParser = require('body-parser');
const check = require('express-validator').check;

const authController = require('../controllers/auth.controller');
const authGuard = require('./guards/auth.guard');

router.get('/signup', authGuard.notAuth, authController.getSignup);

router.post(
    '/signup',
    bodyParser.urlencoded({ extended: true} ),
    check('username')
        .not()
        .isEmpty()
        .withMessage('username is required'),
    check('email')
        .not()
        .isEmpty()
        .withMessage('email is required')
        .isEmail(),
    check('password')
        .isLength({min: 6, max: 12})
        .withMessage('password must be between 6 and 12 characters'),
    check('confirmPassword')
        .custom((value, {req}) => {
            if (value === req.body.password) return true;
            else throw 'password and confirm password do not match';
        }),

    authController.postSignup);

router.get('/login',authGuard.notAuth, authController.getLogin);

router.post(
    '/login',
    bodyParser.urlencoded({ extended: true} ),
    check('email')
    .not()
    .isEmpty()
    .withMessage('email is required'),
check('password')
    .isLength({min: 6, max: 12})
    .withMessage('password must be between 6 and 12 characters'),
    authController.postLogin);

router.all('/logout', authGuard.isAuth, authController.logout);

module.exports = router;