const router = require('express').Router();
const check = require('express-validator').check;
const multer = require('multer');

const adminController = require('../controllers/admin.controller');
const adminGuard = require('./guards/admin.guard');


router.get('/admin/add', adminGuard.isAuth, adminController.getAdd);

router.post('/admin/add',
    adminGuard.isAuth,
    multer({
        storage: multer.diskStorage({
            destination: (req, file, callBack) => {
                callBack(null, 'images')
            },
            filename: (req, file, callBack) =>{
                callBack(null, Date.now()+ '-'+ file.originalname)
            }
        })
    })
    .single('image'),
     adminController.postAdd);

module.exports = router;