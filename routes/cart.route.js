const router = require('express').Router();
const bodyParser = require('body-parser');
const check = require('express-validator').check;

const authGuard = require('../routes/guards/auth.guard');
const cartController = require('../controllers/cart.controller');

router.get('/cart', authGuard.isAuth, cartController.getCart);

router.post('/cart',
  authGuard.isAuth,
  bodyParser.urlencoded({extended: true}),
  check('amount')
  .not()
  .isEmpty()
  .withMessage('amount is required')
  .isInt({int: 1})
  .withMessage('amount should be greater than 0'),
  cartController.postCart
  )

router.post('/cart/save', 
  authGuard.isAuth,
  bodyParser.urlencoded({extended: true}),
  check('amount')
  .not()
  .isEmpty()
  .withMessage('amount is required')
  .isInt({min: 1})
  .withMessage('amount should be greater than 0'),
  cartController.postSaveCart
  )

router.post('/cart/delete',
   authGuard.isAuth,
   bodyParser.urlencoded({extended: true}),
   cartController.postDelete
   )
   
router.post('/cart/deleteAll',
   authGuard.isAuth,
   bodyParser.urlencoded({extended: true}),
   cartController.postDeleteAll
   )

  module.exports = router;